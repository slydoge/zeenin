<?php
	$pages = array("user");
	$prefix = "<b>10:24:2019</b>&nbsp;&nbsp;";
	//$prefix = "<b>" . date("d:m:Y") . "</b> ";
	$brd = "
		Fixed another tiny bug on the site related to clicking links. Thanks for using the site!
	";
	$class = "bg-success pd-10";

	if (in_array($pgName, $pages) || in_array('all', $pages)) {
		echo '
			<div class="container mt-20">
				<div class="paper f-white mb-0 ' . $class . '">
					<p class="justify mb-0">
						' . $prefix . $brd . '
					</p>
				</div>
			</div>
		';
	}
?>