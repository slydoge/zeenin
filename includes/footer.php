<?php
	$json = file_get_contents('https://nodetest.source.dog/?id=233159595261034497');
	$obj = json_decode($json);
	$discord = $obj->username . "#" . $obj->discriminator;
?>
<footer>
	<div class="container">
		<div class="paper pd-10">
			<ul class="menu menu-bleak">
				<li><a href="https://itstep.org/">STEP</a></li>
				<li><a href="https://source.dog"><?php echo $discord; ?></a></li>
				<li><span class="f-gray">Links: <?php echo $totalUrls; ?></span></li>
				<div class="grow"></div>
				<li><a href="interface.php">API</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="tos.php">Terms Of Service</a></li>
				<li><a href="https://twitter.com/p3pprk0mm4ndr" class="f-red-override"><i class="fas fa-heart"></i></a></li>
			</ul>
		</div>
	</div>
</footer>