<nav class="bg-primary">
	<div class="container">
		<ul class="menu nav">
			<li><a href="./">ZEENIN.GA</a></li>
			<div class="grow"></div>
			<?php

				if (isset($_SESSION['username'])&&!empty($_SESSION['username'])) {
					echo '<li><a href="user.php">' . $_SESSION['username'] . '</a></li>';
					echo '<li><a href="logout.php">Logout</a></li>';
				}
				else{
					echo '<li><a href="login.php">Login</a></li>';
				}
				
			?>
		</ul>
	</div>
</nav>