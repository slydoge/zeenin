<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/
	$responseData;

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	function newID($long){
		//set the random id length 
		$random_id_length = $long; 

		//generate a random id encrypt it and store it in $rnd_id 
		$rnd_id = crypt(uniqid(rand(),1)); 

		//to remove any slashes that might have come 
		$rnd_id = strip_tags(stripslashes($rnd_id)); 

		//Removing any . or / and reversing the string .ga
		$rnd_id = str_replace(".","",$rnd_id); 
		$rnd_id = strrev(str_replace("/","",$rnd_id)); 

		//finally I take the first 10 characters from the $rnd_id 
		$rnd_id = substr($rnd_id,0,$random_id_length); 

		return $rnd_id;
	}

	include "includes/connect.php";
	
	if (!empty($_POST)) {
		if (!isset($_POST['key'])) {
			$responseData->success = 0;
			$responseData->message = "ERROR: Missing key";
		}
		else if (!isset($_POST['url'])) {
			$responseData->success = 0;
			$responseData->message = "ERROR: Missing url";
		}
		else if (!isset($_POST['url']) && !isset($_POST['key'])) {
			$responseData->success = 0;
			$responseData->message = "ERROR: Empty request";
		}
		else{
			$stmt = $conn->prepare("SELECT * FROM api_keys WHERE a_key = ? AND valid = '1'");
			$stmt->bind_param("s", $_POST['key']);

			$stmt->execute();
		    $result = $stmt->get_result();
			if ($result->num_rows > 0) {
			    while($row = $result->fetch_assoc()) {
			    	$owner = $row['owner'];
			    	///////////////////////////////////
			    	$parse = parse_url($_POST['url']);

					if (filter_var($_POST['url'], FILTER_VALIDATE_URL) && $parse['host'] != "zeenin.ga") {
						$stmt = $conn->prepare("SELECT ref FROM links WHERE url = ?");
						$stmt->bind_param("s", $_POST['url']);

						$stmt->execute();
				        $result = $stmt->get_result();
						if ($result->num_rows > 0) {
						    while($row = $result->fetch_assoc()) {
								if (isset($_POST['simple'])&&!empty($_POST['simple'])) {
									echo "https://zeenin.ga/" . $row['ref'];
								}
								else{
									$responseData->success = 0;
									$responseData->message = "ERROR: Url already exists";
									$responseData->url = "https://zeenin.ga/" . $row['ref'];
								}
							}
						} else {

							$ranUrl = newID(6);

							if (isset($_POST['ref'])&&!empty($_POST['ref'])) {
								$ranUrl = $_POST['ref'];
							}

							$stmt = $conn->prepare("INSERT INTO links (url, ref, owner) VALUES (?, ?, ?)");
							$stmt->bind_param("sss", $_POST['url'], $ranUrl, $owner);
							if ($stmt->execute()) { 
								if (isset($_POST['simple'])&&!empty($_POST['simple'])) {
									echo "https://zeenin.ga/" . $ranUrl;
								}
								else{
									$responseData->success = 1;
									$responseData->message = "Url successfully created!";
									$responseData->url = "https://zeenin.ga/" . $ranUrl;
								}
							} else {
								$responseData->success = 0;
								$responseData->message = "ERROR: Query failed";
							}
						}

					} else {
						$responseData->success = 0;
						$responseData->message = "ERROR: Invalid url";
					}

			    }
			} 
			else {
				$responseData->success = 0;
				$responseData->message = "ERROR: Invalid key";
			}
		}
		
	} 
	else{

		if (!isset($_GET['key'])) {
			$responseData->success = 0;
			$responseData->message = "ERROR: Missing key";
		}
		else if (!isset($_GET['url'])) {
			$responseData->success = 0;
			$responseData->message = "ERROR: Missing url";
		}
		else if (!isset($_GET['url']) && !isset($_GET['key'])) {
			$responseData->success = 0;
			$responseData->message = "ERROR: Empty request";
		}
		else{
			$stmt = $conn->prepare("SELECT * FROM api_keys WHERE a_key = ? AND valid = '1'");
			$stmt->bind_param("s", $_GET['key']);

			$stmt->execute();
		    $result = $stmt->get_result();
			if ($result->num_rows > 0) {
			    while($row = $result->fetch_assoc()) {
			    	$owner = $row['owner'];
			    	///////////////////////////////////
			    	$parse = parse_url($_GET['url']);

					if (filter_var($_GET['url'], FILTER_VALIDATE_URL) && $parse['host'] != "zeenin.ga") {
						$stmt = $conn->prepare("SELECT ref FROM links WHERE url = ?");
						$stmt->bind_param("s", $_GET['url']);

						$stmt->execute();
				        $result = $stmt->get_result();
						if ($result->num_rows > 0) {
						    while($row = $result->fetch_assoc()) {
								$responseData->success = 0;
								$responseData->message = "ERROR: Url already exists";
								$responseData->url = "https://zeenin.ga/" . $row['ref'];
							}
						} else {

							$ranUrl = newID(6);

							if (isset($_GET['ref'])&&!empty($_GET['ref'])) {
								$ranUrl = $_GET['ref'];
							}

							$stmt = $conn->prepare("INSERT INTO links (url, ref, owner) VALUES (?, ?, ?)");
							$stmt->bind_param("sss", $_GET['url'], $ranUrl, $owner);
							if ($stmt->execute()) { 
								$responseData->success = 1;
								$responseData->message = "Url successfully created!";
								$responseData->url = "https://zeenin.ga/" . $ranUrl;
							} else {
								$responseData->success = 0;
								$responseData->message = "ERROR: Query failed";
							}
						}

					} else {
						$responseData->success = 0;
						$responseData->message = "ERROR: Invalid url";
					}

			    }
			} 
			else {
				$responseData->success = 0;
				$responseData->message = "ERROR: Invalid key";
			}
		}

	}

	if (!isset($_POST['simple'])||empty($_POST['simple'])) {
		$response = json_encode($responseData);

		echo $response;
	}
?>