<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();

	include "includes/connect.php";
	include "includes/check-cookie.php";

	if (isset($_SESSION['username'])&&!empty($_SESSION['username'])) {
		header("location: user.php");
		die();
	}

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	function getRealIpAddr(){
	    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	    {
	      $ip=$_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	    {
	      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else
	    {
	      $ip=$_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}

	function checkPass($pass){
		if (strlen($pass) < '6') {
        	return "Password too short";
    	}
    	else if (!preg_match('/[A-Za-z]/', $pass)) {
    		return "Password must contain letters";
    	}
    	else if (!preg_match('/[0-9]/', $pass)) {
    		return "Password must contain numbers";
    	}
	    else{
	    	return false;
	    }
	}

	if (isset($_POST['username']) && isset($_POST['e-mail']) && isset($_POST['password']) && isset($_POST['password-repeat']) && isset($_POST['tos-box'])) {

		if ($_POST['password']==$_POST['password-repeat']) {

			if (strlen($_POST['username'])<=255) {
			
				if(checkPass($_POST["password"])==false){

					$reservedUsernames = require "includes/reserved-usernames.php";

					if (!in_array($_POST['username'], $reservedUsernames)) {

						if (strlen($_POST['username'])>3&&strlen($_POST['username'])<16) {

							if (filter_var($_POST['e-mail'], FILTER_VALIDATE_EMAIL)) {

								require('recaptcha/autoload.php');

								$gRecaptchaResponse = $_POST['g-recaptcha-response'];
								$remoteIp = getRealIpAddr();
								
								$recaptcha = new \ReCaptcha\ReCaptcha("6LdvF1kUAAAAANmI7vqToDLGKeyk-KZeYRynNPe6");
								$resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);

								if ($resp->isSuccess()) {
									if($resp->getHostName() === $_SERVER['SERVER_NAME']) {

										$stmt = $conn->prepare("SELECT id FROM tbl_users WHERE username = ?");
										$stmt->bind_param("s", $_POST['username']);

										$stmt->execute();
								        $result = $stmt->get_result();
										if ($result->num_rows > 0) { $val_error = "Username not available"; } 
										else {
											$stmt = $conn->prepare("INSERT INTO tbl_users (username, email, password) VALUES (?, ?, ?)");

											$hash = password_hash($_POST['password'], PASSWORD_DEFAULT);

											$stmt->bind_param("sss", $_POST['username'], $_POST['e-mail'], $hash);
											if ($stmt->execute()) {
												$_SESSION['username'] = $_POST['username'];
												header("location: user.php");
												die();
											} 
											else { $val_error = $stmt->error; }
										}

									}
									else{
					    				$val_error = "Invalid reCAPTCHA domain";
									}
								} else {
					    			$val_error = "Invalid reCAPTCHA";
								    $errors = $resp->getErrorCodes();
								    echo $errors;
								}
							}
							else{ $val_error = "Invalid e-mail address"; }
						}
						else{ $val_error = "Username must be between 4 and 16 symbols long"; }
					}
					else{ $val_error = "Username not available"; }
				}
				else{ $val_error = checkPass($_POST["password"]); }
			}
			else{ $val_error = "Username is too long"; }
		}
		else{ $val_error = "Passwords do not match"; }

	}
	else if (!isset($_POST['username']) && !isset($_POST['e-mail']) && !isset($_POST['password']) && !isset($_POST['password-repeat']) && !isset($_POST['tos-box'])) { /*no submitted fields*/ }
	else{
		$val_error = "All the fields are required";
	}

	//page data
	$pgName = "register";

?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow flex flex-center">
		<div class="container-small mt-20">
			<?php
				if (!empty($val_error)) {
					echo '
						<div class="paper bg-warning f-white">
							' . $val_error . '
						</div>
					';
				}
			?>
			<div class="paper">
				<form action="register.php" method="POST" class="form" id="reform">
					<center>Register</center>
					<br>
					<input type="text" name="username" class="block" placeholder="Username">
					<br>
					<input type="email" name="e-mail" class="block" placeholder="E-mail">
					<br>
					<input type="password" name="password" class="block" placeholder="Password">
					<br>
					<input type="password" name="password-repeat" class="block" placeholder="Repeat password">
					<br>
					<div class="flex flex-middle remember-me">
						<input id="tos-box" class="mr-5" type="checkbox" name="tos-box">
						<label for="tos-box" class="flex flex-vert-middle">I agree to the <a href="tos.php">Terms Of Service</a></label>
					</div>
					<br>
					<button class="g-recaptcha submit-input btn btn-primary block" data-sitekey="6LdvF1kUAAAAAM40GvkdT-pYuw4gaImptt6_BSzi" data-callback="rec" data-badge="invisible">Let's go</button>
					<br>
					<center><a href="./login.php">Already have an account?</a></center>
				</form>
			</div>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
	<script type="text/javascript">
		function rec(){ document.getElementById("reform").submit(); }
	</script>
</body>
</html>