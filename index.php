<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();

	include "includes/connect.php";
	include "includes/check-cookie.php";

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	function newID($long){
		$random_id_length = $long;
		$rnd_id = crypt(uniqid(rand(),1));
		$rnd_id = strip_tags(stripslashes($rnd_id));
		$rnd_id = str_replace(".","",$rnd_id); 
		$rnd_id = strrev(str_replace("/","",$rnd_id));
		$rnd_id = substr($rnd_id,0,$random_id_length); 
		return $rnd_id;
	}

	//page data
	$pgName = 'index';
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link href="assets/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow flex flex-center">
		<div class="container-small mt-20">
			<div class="paper">
				<form action="./" method="POST" class="form">
					<?php
						if (isset($_GET['r'])&&!empty($_GET['r'])) {

							$stmt = $conn->prepare("SELECT url FROM links WHERE ref = ?");
							$stmt->bind_param("s", $_GET['r']);
							$stmt->execute();
					        $result = $stmt->get_result();

							if ($result->num_rows > 0) {

							    while($row = $result->fetch_assoc()) {

							    	$goto = $row['url'];
									$stmt = $conn->prepare("UPDATE links SET visits=visits+1 WHERE url = ?");
									$stmt->bind_param("s", $goto);

									if ($stmt->execute()) {

								    	header("Location: ". $goto);
										exit();

									}

							    }
							} 
							else {

								echo '
									<center class="f-dark">
										Sorry, link not found!
									</center>
									<br>
									<a href="https://zeenin.ga">
										<button class="btn btn-primary block">Wanna make a new one?</button>
									</a>
								';

							}
						}
						else if (isset($_POST['link'])&&!empty($_POST['link'])) {

							$parse = parse_url($_POST['link']);

							if (filter_var($_POST['link'], FILTER_VALIDATE_URL) && $parse['host'] != "zeenin.ga") {

								$stmt = $conn->prepare("SELECT ref FROM links WHERE url = ?");
								$stmt->bind_param("s", $_POST['link']);
								$stmt->execute();
						        $result = $stmt->get_result();

								if ($result->num_rows > 0) {

								    while($row = $result->fetch_assoc()) {

								    	echo '
											<center class="f-dark">
												<p>Your url already exists here:</p>
												<div class="copy-me">
													<input type="text" name="link" class="block" value="https://zeenin.ga/' . $row['ref'] . '" readonly>
													<div class="hover-icon dropdown-icon">
														<i class="fas fa-angle-down dropdown-icon f-blue"></i>
													</div>
													<div class="hover-icon copy-icon">
														<i class="far fa-copy copy-icon f-blue"></i>
													</div>
													<div class="copy-dropdown">
														<ul>
															<li class="domain-option selected">zeenin.ga</li>
															<li class="domain-option">zeenin.ml</li>
															<li class="domain-option">ze.wtf</li>
														</ul>
													</div>
												</div>
											</center>
											<br>
											<a href="https://zeenin.ga">
												<button class="btn btn-primary block">Wanna make a new one?</button>
											</a>
											';
											/*
										echo '
											<center class="f-dark">
												Your url already exists here:
												<br>
												<br>
												<div class="copy-me">
													<input type="text" name="link" class="block" value="https://zeenin.ga/' . $row['ref'] . '" readonly>
													<i class="far fa-copy f-blue"></i>
												</div>
											</center>
											<br>
											<a href="httpss://zeenin.ga/">
												<button class="btn btn-primary block">Wanna make a new one?</button>
											</a>
										';*/

									}

								} 
								else {

									$owner = 0;

									if (isset($_SESSION['username'])) {

										$stmt = $conn->prepare("SELECT id FROM tbl_users WHERE username = ?");
										$stmt->bind_param("s", $_SESSION['username']);
										$stmt->execute();
								        $result = $stmt->get_result();

										if ($result->num_rows > 0) {

										    while($row = $result->fetch_assoc()) { 

										    	$owner = $row['id']; 

										    }

										}

									}

									$ranUrl = newID(6);

									if (isset($_POST['custom'])&&isset($_SESSION['username'])&&!empty($_SESSION['username'])&&strlen(trim($_POST['custom']))>0) {

										$ranUrl = trim($_POST['custom']);

									}

									$stmt = $conn->prepare("SELECT id FROM links WHERE ref = ?");
									$stmt->bind_param("s", $ranUrl);
									$stmt->execute();
							        $result = $stmt->get_result();

									if ($result->num_rows == 0) {

										$stmt = $conn->prepare("INSERT INTO links (url, ref, owner) VALUES (?, ?, ?)");
										$stmt->bind_param("sss", $_POST['link'], $ranUrl, $owner);

										if ($stmt->execute()) { 

											echo '
												<center class="f-dark">
													Success! Your url is available here:
													<br>
													<br>
													<div class="copy-me">
														<input type="text" name="link" class="block" value="https://zeenin.ga/' . $ranUrl . '" readonly>
														<div class="hover-icon dropdown-icon">
															<i class="fas fa-angle-down dropdown-icon f-blue"></i>
														</div>
														<div class="hover-icon copy-icon">
															<i class="far fa-copy copy-icon f-blue"></i>
														</div>
														<div class="copy-dropdown">
															<ul>
																<li class="domain-option selected">zeenin.ga</li>
																<li class="domain-option">zeenin.ml</li>
																<li class="domain-option">ze.wtf</li>
															</ul>
														</div>
													</div>
												</center>
												<br>
												<a href="https://zeenin.ga">
													<button class="btn btn-primary block">Wanna make a new one?</button>
												</a>
											';

										} 
										else {

											echo '
												<center class="f-dark">Sorry, some kinda error happened :/</center>
												<div class="paper bg-gray mt-20">' . $stmt->error . '</div>
												<a href="https://zeenin.ga">
													<button class="btn btn-primary block">Wanna make a new one?</button>
												</a>
											';

										}

									}
									else{
										//url taken

										echo '
											<center class="f-dark">Sorry, some kinda error happened :/</center>
											<div class="paper bg-gray mt-20">URL occupied</div>
											<a href="https://zeenin.ga">
												<button class="btn btn-primary block">Wanna make a new one?</button>
											</a>
										';
									}
								}

							} 
							else {

								/*
							if (filter_var($_POST['link'], FILTER_VALIDATE_URL) && $parse['host'] != "zeenin.ga") {*/
								//echo $_POST['link'];
								echo '
									<center class="f-dark">Sorry, some kinda error happened :/</center>
									<div class="paper bg-gray mt-20">ERROR: Invalid URL.</div>
									<a href="https://zeenin.ga">
										<button class="btn btn-primary block">Wanna make a new one?</button>
									</a>
								';

							}

						}
						else if ((isset($_POST['link'])&&empty($_POST['link'])) || !isset($_POST['link'])) {
							//link is empty after redir
							echo '
								<center class="f-dark">Enter your long url to shorten it!</center>
								<br>
								<input type="text" name="link" class="block" placeholder="Link goes here">
								<br>
							';

							if (isset($_SESSION['username'])&&!empty($_SESSION['username'])) {

								echo '
									<input type="text" name="custom" class="block" placeholder="Custom ID (optional)">
									<br>
								';

							}

							echo '
								<input type="submit" name="submit" class="block" value="Let\'s go">
							';

						}

						$stmt->close();
						$conn->close();
					?>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function copyToClipboard(text) {
		    var selected = false;
		    var el = document.createElement('textarea');
		    el.value = text;
		    el.setAttribute('readonly', '');
		    el.style.position = 'absolute';
		    el.style.left = '-9999px';
		    document.body.appendChild(el);
		    if (document.getSelection().rangeCount > 0) {
		        selected = document.getSelection().getRangeAt(0)
		    }
		    el.select();
		    document.execCommand('copy');
		    document.body.removeChild(el);
		    if (selected) {
		        document.getSelection().removeAllRanges();
		        document.getSelection().addRange(selected);
		    }
		}

		var open = false;

		$('html').click(function(e) {
			console.log("hi");
			if($(e.target).hasClass('dropdown-icon')&&!open){
				$(".copy-dropdown").slideDown(200);
				open = true;
			}
			else{
				$(".copy-dropdown").slideUp(200); 
				open = false;   
			}
		});

		$(".domain-option").click(function(){
			var n = $(".copy-me>input").val().lastIndexOf('/');
			var result = $(".copy-me>input").val().substring(n);
			$(".copy-me>input").val("https://" + $(this).text() + result);
		});

		$(".copy-icon").click(function(){
			copyToClipboard($(".copy-me>input").val());
		});

		//this fixes the url resubmition bug
		$("a").click(function(e){
			e.preventDefault();
			window.location.href = $(this).attr("href");
		});
	</script>
	<?php include "includes/footer.php"; ?>
</body>
</html>