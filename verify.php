<?php
	
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	session_start();

	if (!isset($_SESSION['username'])||empty($_SESSION['username'])) {
		header("location: login.php");
		die();
	}

	include "includes/connect.php";

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	//page data
	$pgName = "verify";

?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow flex flex-center">
		<div class="container-small mt-20">
			<div class="paper">
				<center>
					<?php 
						if (isset($_POST['email'])&&!empty($_POST['email'])&&filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

							$hash = md5(rand(0,1000));

							$stmt = $conn->prepare("UPDATE tbl_users SET email = ?, hash = ? WHERE username = ?");
							$stmt->bind_param("sss", $_POST['email'], $hash, $_SESSION['username']);

							if ($stmt->execute()) {
								$to      = $_POST['email'];
								$subject = 'Email verification';
								$message = '
								 
								Thanks for signing up with zeenin.ga! Here is your email verification link:
								 
								https://www.zeenin.ga/verify.php?hash='.$hash.'
								 
								';
								                     
								$headers = 'From:noreply@zeenin.ga' . "\r\n";
								mail($to, $subject, $message, $headers);

								echo "E-mail verification link sent! Don't forget to check your spam folder!<br><br><a href='./'>Back to the site</a>";
							} 
							else {
								echo "error update";
								return;
							}
						}
						else if (isset($_GET['hash'])&&!empty($_GET['hash'])) {
							$stmt = $conn->prepare("UPDATE tbl_users SET verified = 1, hash = '' WHERE hash = ?");
							$stmt->bind_param("s", $_GET['hash']);

							if ($stmt->execute()) {
								echo "E-mail verified!<br><br><a href='./'>Back to the site</a>";
							} 
							else {
								echo "error update";
								return;
							}
						}
						else{
					    	header("Location: index.php");
							exit();
						}

						$conn->close();
					?>
				</center>
			</div>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
</body>
</html>