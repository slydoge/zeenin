<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();

	include "includes/connect.php";

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	//page data
	$pgName = "tos";
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link href="assets/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow">
		<div class="container mt-20">
			<div class="paper f-dark">
				<center><h2>Terms Of Service</h2></center>
				<br>
				<p>These terms and conditions outline the rules and regulations for the use of this website. By accessing this website I assume you accept these terms and conditions in full. Do not continue to use this website if you do not accept all of the terms and conditions stated on this page.</p>
				<br>
				<ul class="ul">
					<li>The website staff reserves the right to remove any entries it considers to be against the site rules.</li>
					<li>The website staff reserves the right to modify and update the website rules and terms of service at any given time.</li>
					<li>The website staff reserves the right to deny access to the site to anyone who is considered to be breaking the site rules.</li>
					<li>The website staff does not disclose and will never disclose your personal information to any third parties without your direct consent.</li>
					<li>Any content that is considered to be against the law will be moderated regardless of whether it abides by the site rules or not.</li>
					<li>The website is provided as-is and I am not liable for any potential damage caused by it, or by the website visitors.</li>
				</ul>
				<br>
				<center><h2>Site rules</h2></center>
				<br>
				<p>These are the rules about basic usage of the site. Breaking them will get your access to the site restricted.</p>
				<br>
				<ol class="ol">
					<li>No links leading to phishing pages or similar security exploits are allowed.</li>
					<li>No links leading to any illegal content, such as child pornography or illegal produce retailers are allowed.</li>
					<li>Any sort of discrimination or hate speech is not allowed on the site.</li>
				</ol>
			</div>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
</body>
</html>