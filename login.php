<?php
	
	//ini_set('display_errors', 1);
	//ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL);

	session_start();

	include "includes/connect.php";
	include "includes/check-cookie.php";
	
	if (isset($_SESSION['username'])&&!empty($_SESSION['username'])) {
		header("location: user.php");
		die();
	}

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	function getRealIpAddr(){
	    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	    {
	      $ip=$_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	    {
	      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else
	    {
	      $ip=$_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}
	function newID($long){
		$random_id_length = $long;
		$rnd_id = crypt(uniqid(rand(),1));
		$rnd_id = strip_tags(stripslashes($rnd_id));
		$rnd_id = str_replace(".","",$rnd_id); 
		$rnd_id = strrev(str_replace("/","",$rnd_id));
		$rnd_id = substr($rnd_id,0,$random_id_length); 
		return $rnd_id;
	}

	if (isset($_POST['username']) && isset($_POST['password'])) {

		require('recaptcha/autoload.php');

		$gRecaptchaResponse = $_POST['g-recaptcha-response'];
		$remoteIp = getRealIpAddr();
		
		$recaptcha = new \ReCaptcha\ReCaptcha("6LctG1kUAAAAAKjXtgYhy-Mx39r_cO4-SaUmdSRF");
		$resp = $recaptcha->verify($gRecaptchaResponse, $remoteIp);

		if ($resp->isSuccess()) {
			if($resp->getHostName() === $_SERVER['SERVER_NAME']) {

				$stmt;

				if(filter_var($_POST['username'], FILTER_VALIDATE_EMAIL)){
					$stmt = $conn->prepare("SELECT id, username, password FROM tbl_users WHERE email = ?");
					$stmt->bind_param("s", $_POST['username']);
				}
				else
				{
					$stmt = $conn->prepare("SELECT id, username, password FROM tbl_users WHERE username = ?");
					$stmt->bind_param("s", $_POST['username']);
				}

				$stmt->execute();
		        $result = $stmt->get_result();
				if ($result->num_rows > 0) {
				    while($row = $result->fetch_assoc()) {
				    	if (password_verify($_POST['password'], $row['password'])) {
				    		//successfull login
				    		if (isset($_POST['remember'])&&$_POST['remember']=="1") {
				    			//checkbox was checked
				    			$cookie_name = "remember";

				    			$ck_id = newID(12);
				    			$ck_token = newID(12);
				    			$ck_hash = hash('sha256', $ck_token);

								$stmt = $conn->prepare("INSERT INTO tokens (token, user, selector) VALUES (?, ?, ?)");
								$stmt->bind_param("sss", $ck_hash, $row['id'], $ck_id);

								if ($stmt->execute()) { 
									$cookie_value = $ck_id . " " . $ck_token;
									setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
								}
								else{
									$val_error = "Failed to set remember cookie";
								}

				    		}
				    		//set session and proceed
							$_SESSION['username'] = $row['username'];
							header("location: user.php");
							die();
				    	}
				    	else{
				    		//bad pass
				    		$val_error = "Incorrect password";
				    	}
				    }
				} 
				else {
					//no such username
					$val_error = "User doesn't exist";
				}

			}
			else{
				$val_error = "Invalid reCAPTCHA domain";
			}
		} else {
			$val_error = "Invalid reCAPTCHA";
		    $errors = $resp->getErrorCodes();
		    echo $errors;
		}
	}

	//page data
	$pgName = "login";

?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow flex flex-center">
		<div class="container-small mt-20">
			<?php
				if (!empty($val_error)) {
					echo '
						<div class="paper bg-warning f-white">
							' . $val_error . '
						</div>
					';
				}
			?>
			<div class="paper">
				<form action="login.php" method="POST" id="reform" class="form">
					<center>Login</center>
					<br>
					<input type="text" name="username" class="block" placeholder="Username or E-mail">
					<br>
					<input type="password" name="password" class="block" placeholder="Password">
					<br>
					<div class="flex flex-middle remember-me">
						<input type="checkbox" class="mr-5" name="remember" id="remme" value="1">
						<label for="remme" class="flex flex-vert-middle">Remember me</label>
					</div>
					<br>
					<button class="g-recaptcha submit-input btn btn-primary block" data-sitekey="6LctG1kUAAAAAOXzwpsCtpASrmHb7lFDTf3TSQiT" data-callback="rec" data-badge="invisible">Let's go</button>
					<br>
					<center><a href="./register.php">Don't have an account?</a></center>

					<?php $conn->close(); ?>
				</form>
			</div>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
	<script type="text/javascript">
		function rec(){ document.getElementById("reform").submit(); }
	</script>
</body>
</html>