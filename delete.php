<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();
	
	include "includes/connect.php";

	if (!isset($_SESSION['username'])||empty($_SESSION['username'])) {
		header("location: login.php");
		die();
	}

	include "includes/connect.php";

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	$uid;
	$email;
	$verified;

	$stmt = $conn->prepare("SELECT id, email, verified FROM tbl_users WHERE username = ?");
	$stmt->bind_param("s", $_SESSION['username']);

	$stmt->execute();
    $result = $stmt->get_result();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) {
	    	$uid = $row['id'];
	    	$email = $row['email'];
	    	$verified = $row['verified'];
	    }
	} 
	else {
		echo "nouser";
		return;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Zeenin</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php include "includes/header.php"; ?>
	<div class="grow">
		<div class="central">
			<form action="./" method="POST">
			<?php

				if (isset($_GET['id'])&&!empty($_GET['id'])) {
					$urlId = $_GET['id'];

					$stmt = $conn->prepare("SELECT owner FROM links WHERE id = ?");
					$stmt->bind_param("s", $urlId);

					$stmt->execute();
			        $result = $stmt->get_result();
					if ($result->num_rows > 0) {
					    while($row = $result->fetch_assoc()) {

					    	if($uid == $row['owner']){
								$stmt = $conn->prepare("DELETE FROM links WHERE id = ?");
								$stmt->bind_param("s", $urlId);

								$goto = "./user.php";

								if ($stmt->execute()) {
							    	header("Location: ". $goto);
									exit();
									//echo "Attempted delete of " . $urlId;
								} 
								else {
									echo "error delete";
									return;
								}
					    	}
					    	else{
					    		echo "Not owner";
					    	}

					    }
					} else {
						echo "Other error";
					}
				}
				else{
						echo "Other error1";

				}

				$conn->close();
			?>
			</form>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
</body>
</html>