<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/
	include "includes/connect.php";

	session_start();

	if(isset($_COOKIE['remember']) && isset($_SESSION['username'])) {

		$ck_data = explode(" ", $_COOKIE['remember']);
		$ck_hash = hash('sha256', $ck_data[1]);

		$sess_name = $_SESSION['username'];

		$stmt = $conn->prepare("SELECT id FROM tbl_users WHERE username = ?");
		$stmt->bind_param("s", $sess_name);

		$stmt->execute();
		$result = $stmt->get_result();

		if ($result->num_rows > 0) {

		    while($row = $result->fetch_assoc()) {

		    	$stmt = $conn->prepare("DELETE FROM tokens WHERE user = ? AND selector = ?");
				$stmt->bind_param("ss", $row['id'], $ck_data[0]);
				$stmt->execute();
				$result = $stmt->get_result();

				if ($stmt->execute()) {

					setcookie("remember", "", time() - 3600);
					$conn->close();
					session_unset();
					session_destroy();
					header("location: index.php");
					die();

				}
				else{

					echo $stmt->error;

				}

			}
		}
		else {

			echo "no user?";

		}
		
	}

	$conn->close();
	session_unset();
	session_destroy();
	header("location: index.php");
	die();

?>