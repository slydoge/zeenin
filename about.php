<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();

	include "includes/connect.php";

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	//page data
	$pgName = "about";

?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link href="assets/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow">
		<div class="container mt-20">
			<div class="paper f-dark">
				Welcome to ZEENIN.GA! This is a little URL shortener made for STEP students by a STEP student. You are welcome to enjoy this even if you aren't a STEP student though :P
				<br><br>
				<center><h2>FAQ</h2></center>
				<br><br>
				<div class="paper bg-gray pd-10">
					Q: What engine is this based on?
				</div>
				<div class="paper pd-10">
					A: Custom-written from scratch.
				</div>
				<div class="paper bg-gray pd-10">
					Q: What languages did you use?
				</div>
				<div class="paper pd-10">
					A: PHP for backend, JS for basic front-end bits and standard HTML/CSS.
				</div>
				<div class="paper bg-gray pd-10">
					Q: Do you pay for hosting?
				</div>
				<div class="paper pd-10">
					A: Yes
				</div>
				<div class="paper bg-gray pd-10">
					Q: Is my data safe?
				</div>
				<div class="paper pd-10">
					A: User passwords are BCrypt encrypted. I'm planning to add e-mail address encryption in the near future. Your data is not shared with any third parties with the exception of some additional data being collected by reCAPTCHA (<a href="https://termsfeed.com/blog/privacy-policy-recaptcha/">Read more</a>).
				</div>
				<div class="paper bg-gray pd-10">
					Q: I have a complaint/question/suggestion.
				</div>
				<div class="paper pd-10 mb-0">
					A: Feel free to bug me on Discord, which can be found below, in the site's footer.</u>
				</div>
			</div>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
</body>
</html>