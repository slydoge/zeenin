<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();

	include "includes/connect.php";

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	//page data
	$pgName = "interface";

?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link href="assets/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow">
		<div class="container mt-20">
			<div class="paper f-dark">
				<center><h2>API</h2></center>
				<br>
				<p>Our API is fairly simple! First, to access it from your application, you will need an API key. You can generate or change your API key on your <a href="./user.php">user page.</a><br><br>Once you have the key, all you need to do is make a proper request to our API page. Our API accepts both GET and POST methods. All you have to do is pass your key and the URL that you want to shorten. Here is an example of how you would do it with the GET method:</p>
				<br>
				<div class="paper bg-gray pd-10">https://zeenin.ga/api.php?key=<span class="f-gray">&lt;YOUR_KEY_HERE&gt;</span>&url=<span class="f-gray">&lt;YOUR_LONG_URL&gt;</span></div>
				<p>Additionally, you can specify a "ref" parameter to add a custom id to your url:</p>
				<br>
				<div class="paper bg-gray pd-10">https://zeenin.ga/api.php?key=<span class="f-gray">&lt;YOUR_KEY_HERE&gt;</span>&url=<span class="f-gray">&lt;YOUR_LONG_URL&gt;</span>&ref=<span class="f-gray">&lt;YOUR_CUSTOM_ID&gt;</span></span></div>
				<p>If you have done it correctly, your application will receive a JSON response that looks something like this:</p>
				<br>
				<div class="paper bg-gray pd-10">
					<pre>
{
	success: 1,
	url: https://zeenin.ga/<span class="f-gray">&lt;YOUR_NEW_URL&gt;</span>,
	message: "URL shortened successfully."
}</pre>
				</div>
				<p>If you haven't however, the response will make sure to tell you why. For example, this is what you will get if you forget to pass your key:</p>
				<br>
				<div class="paper bg-gray pd-10">
					<pre>
{
	success: 0,
	message: "ERROR: Missing API key"
}</pre>
				</div>
				<p>You can also specify an additonal parameter called "simple". This will make the page return the shortened URL in case of successful insert or 0 in case of an error. <br><br>There are a total of 6 error messages you can receive, all of which are pretty self-explanatory:</p>
				<br>
				<ul class="ul">
					<li>Missing key</li>
					<li>Missing url</li>
					<li>Invalid key</li>
					<li>Invalid url</li>
					<li>Empty request</li>
					<li>Url exists</li>
					<li>Query failed</li>
				</ul>
				<br>
				<p>Happy programming! This API is still in early development, so if you have any questions or feedback feel free to contact me on Discord, which can be found below, in the site's footer.</p>
			</div>
		</div>
	</div>
	<?php include "includes/footer.php"; ?>
</body>
</html>