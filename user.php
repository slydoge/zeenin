<?php
	/*
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	*/

	session_start();

	include "includes/connect.php";
	include "includes/check-cookie.php";

	if (!isset($_SESSION['username'])||empty($_SESSION['username'])) {
		header("location: login.php");
		die();
	}

	$stmt = $conn->prepare("SELECT id FROM links");
	$stmt->execute();
    $result = $stmt->get_result();
	$totalUrls = $result->num_rows;

	$uid;
	$email;
	$verified;
	$al;

	$stmt = $conn->prepare("SELECT id, email, verified, access_level FROM tbl_users WHERE username = ?");
	$stmt->bind_param("s", $_SESSION['username']);

	$stmt->execute();
    $result = $stmt->get_result();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) {
	    	$uid = $row['id'];
	    	$email = $row['email'];
	    	$verified = $row['verified'];
	    	$al = $row['access_level'];
	    }
	} 
	else {
		echo "nouser";
		return;
	}

	$api_key = "";

	$stmt = $conn->prepare("SELECT a_key FROM api_keys WHERE owner = ?");
	$stmt->bind_param("s", $uid);

	$stmt->execute();
    $result = $stmt->get_result();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) {
	    	$api_key = $row['a_key'];
	    }
	}

	$goto = "";

	if (isset($_POST['get-key'])&&!empty($_POST['get-key'])) {
		$hash = md5(rand(0,1000));

		$stmt = $conn->prepare("UPDATE api_keys SET valid = 0 WHERE owner = ?");
		$stmt->bind_param("s", $uid);

		$stmt->execute();

		//if has verified mail
		if ($verified == 1) {
			$stmt = $conn->prepare("INSERT INTO api_keys (owner, a_key) VALUES (?, ?)");
			$stmt->bind_param("ss", $uid, $hash);
			if ($stmt->execute()) { 
				//$goto = "api";
				$api_key = $hash;

			} else {
				echo "Error";
			}
		}
	}

	//page data
	$pgName = "user";

?>
<!DOCTYPE html>
<html>
<head>
	<title>ZEENIN.GA</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" sizes="16x16" href="./res/favicon-16x16.png">
	<link href="assets/fontawesome-free-5.6.3-web/css/all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php 
		include "includes/header.php"; 
		include "includes/broadcast.php";
	?>
	<div class="grow">
		<div class="container mt-20">
			<div class="paper flex">
				<!--
				<div class="sidebar">
					<ul class="menu side-menu">
						<li><a class="bg-gray" href="./">test</a></li>
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</div>
				-->
				<div class="grow max-full">
					<div class="paper bg-gray pd-10"><center class="f-dark">Links</center></div>
					<?php
						$length = 10;
						$page_range = 5;
						$page = 0;
						$totalRows;
						$sort = "date";

						if(isset($_GET['page'])){ $page = $_GET['page']; }
						if(isset($_GET['sort'])){
							if ($_GET['sort']=='d') {
								$sort = "date";
							}
							else if ($_GET['sort']=='v') {
								$sort = "visits";
							}
							else if ($_GET['sort']=='r') {
								$sort = "ref";
							}
							else if ($_GET['sort']=='u') {
								$sort = "url";
							}
						}

						$offset = $page * $length;

						$stmt = $conn->prepare("SELECT *, (SELECT COUNT(*) FROM links WHERE owner = ?) AS total FROM links WHERE owner = ? ORDER BY " . $sort . " DESC LIMIT ?, ?");

						$stmt->bind_param("ssss", $uid, $uid, $offset, $length);

						$stmt->execute();
				        $result = $stmt->get_result();
						if ($result->num_rows > 0) {
							echo "<div class='wide-holder'><table class='link-table'><tbody>";
							echo "<tr>
								    <th><a href='user.php?sort=u' class='clear-a'>Original</a></th>
								    <th><a href='user.php?sort=d' class='clear-a'>Created</a></th>
								    <th><a href='user.php?sort=r' class='clear-a'>Short</a></th>
								    <th><a href='user.php?sort=v' class='clear-a'>Visits</a></th>
								    <th></th>
								  </tr>";
						    while($row = $result->fetch_assoc()) {
								$totalRows = $row['total'];
								echo '
								  <tr>
								  	<td><a href="' . $row['url'] . '">' . $row['url'] . '</a></td>
								  	<td>' . date('M j, Y', strtotime($row['date'])) . '</td>
								  	<td><a href="https://zeenin.ga/' . $row['ref'] . '">https://zeenin.ga/' . $row['ref'] . '</a></td>
								  	<td>' . $row['visits'] . '</td>
								  	<td>
								  		<a href="delete.php?id=' . $row['id'] . '" class="f-red delete-button" onclick="return confirm(`Are you sure? It can\'t be undone later!`)">
								  			<!--<img src="https://cdn3.iconfinder.com/data/icons/faticons/32/remove-01-512.png" width="16px" height="16px">-->
							  				<i class="fas fa-times"></i>
								  		</a>
								  	</td>
								  </tr>';
							}
							echo "</tbody></table></div>";

							//pagination
							
							$numvar = 0;

							$checkpages = ceil($totalRows / $length);

							if ($checkpages>1) {
								$lastpage = $checkpages - 1;
								$previous = $page - 1;
								$checklast = $checkpages -1;
								$next = $page + 1;
								echo "<center class='mt-20'><ul class='page-numbers'>";
								if($page > 0){
									echo "<li><a href='user.php?sort=$sort&page=0'>First</a></li>";
									echo "<li><a href='user.php?sort=$sort&page=$previous'>Prev</a></li>";
								}
								$range_center = $page+1;
								$range_start = $range_center-(($page_range-1)/2);
								$range_end = $range_center+(($page_range-1)/2);
								//fix this please you stupid idiot
								if ($range_start<=0) {
									$range_start = 1;
									if ($page_range>$checkpages) {
										$range_end = $checkpages;
									}
									else{
										$range_end = $page_range;
									}
								}
								if ($range_end>$checkpages) {
									$range_end = $checkpages;
									
									$range_start = $checkpages-$page_range+1;
									if ($range_start<=0) {
										$range_start=1;
									}
								}
								//left ellipse
								if ($range_start > 1) {
									echo "<li><span>...</span></li>";
								}
								for ($i=$range_start; $i <= $range_end; $i++) { 
									$pnum = $i-1;
									$strlink1 = "<li><a href='user.php?sort=$sort&page=$pnum'>";
									$strlink2 = "</a></li>";
									if($page == $pnum){
										$strlink1 = "<li class='p-current'><a href='user.php?sort=$sort&page=$pnum'>";
										$strlink2 = "</a></li>";
									}
									echo "$strlink1$i$strlink2";
								}
								//right ellipse
								if ($range_end < $checkpages) {
									echo "<li><span>...</span></li>";
								}
								if($page != $checklast){
									echo "<li class=''><a href='user.php?sort=$sort&page=$next'>Next</a></li>";
									echo "<li class=''><a href='user.php?sort=$sort&page=$lastpage'>Last</a></li>";
								}
								echo "</ul></center>";
							}
						} else {
							echo '<p><center>You have not created any links! <a href="./">Wanna go make one?</a></center></p>';
						}
					?>
					<div class="paper bg-gray pd-10 mt-20"><center class="f-dark">Settings</center></div>
					<?php
						if (!$verified) {
							echo '<div class="paper bg-warning f-white pd-10">You must have your E-mail verified to generate an API key.</div>';
						}
					?>
					<div class="flex wrap split-equal">
						<div class="grow">
							<form method="POST" action="verify.php" class="flex form wrap newline-wrap">
								<?php 
									echo '<input type="text" placeholder="E-Mail" value="' . $email . '" class="grow mr-10" name="email">';
									if (!$verified) {
										echo '<input type="submit" name="submit" value="Send verification letter">';
									}
									else{
										echo '<input type="submit" name="submit" value="E-mail verified!">';
									}
								?>
							</form>
						</div>
						<div class="grow">
							<form method="POST" action="user.php" class="flex form wrap newline-wrap">
								<?php echo '<input type="text" value="' . $api_key . '" placeholder="API key" class="grow mr-10" name="apikey" readonly>'; ?>
								<input type="hidden" name="get-key" value="1">
								<input type="submit" name="submit" value="Generate new key">
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php

			if ($al==10) {

				echo '
					<div class="container mt-20">
						<div class="paper flex">
							<div class="grow max-full">
				';
				/*
				echo '
								<div class="paper bg-gray pd-10"><center class="f-dark">Broadcast</center></div>
								Make a new broadcast here
								<br><br>
								<div class="paper bg-gray pd-10"><center class="f-dark">Post</center></div>
								Make a new post here
								<br><br>
				';
				*/
				echo '<div class="paper bg-gray pd-10"><center class="f-dark">Recently created links</center></div>';

				$stmt = $conn->prepare("SELECT * FROM links ORDER BY date DESC LIMIT 5");
				
				$stmt->execute();
		        $result = $stmt->get_result();

				if ($result->num_rows > 0) {
					echo "<div class='wide-holder'><table class='link-table'><tbody>";
					echo "<tr>
						    <th>Original</th>
						    <th>Created</th>
						    <th>Short</th>
						    <th>Visits</th>
						  </tr>";
				    while($row = $result->fetch_assoc()) {
						echo '
						  <tr>
						  	<td><a href="' . $row['url'] . '">' . $row['url'] . '</a></td>
						  	<td>' . date('M j, Y', strtotime($row['date'])) . '</td>
						  	<td><a href="https://zeenin.ga/' . $row['ref'] . '">https://zeenin.ga/' . $row['ref'] . '</a></td>
						  	<td>' . $row['visits'] . '</td>
						  </tr>';
					}
					echo "</tbody></table></div>";
				}
				echo '<br><div class="paper bg-gray pd-10"><center class="f-dark">Recently registered users</center></div>';

				$stmt = $conn->prepare("SELECT * FROM tbl_users ORDER BY date_joined DESC LIMIT 5");
				
				$stmt->execute();
		        $result = $stmt->get_result();

				if ($result->num_rows > 0) {
					echo "<div class='wide-holder'><table class='link-table user-table'><tbody>";
					echo "<tr>
						    <th>E-mail</th>
						    <th>Date joined</th>
						    <th>Username</th>
						    <th>Verified</th>
						  </tr>";
				    while($row = $result->fetch_assoc()) {
						echo '
						  <tr>
						  	<td>' . $row['email'] . '</td>
						  	<td>' . date('M j, Y', strtotime($row['date_joined'])) . '</td>
						  	<td>' . $row['username'] . '</td>
						  	<td>' . $row['verified'] . '</td>
						  </tr>';
					}
					echo "</tbody></table></div>";
				}

				/*
				echo '
								<br><br>
								<div class="paper bg-gray pd-10"><center class="f-dark">Stats</center></div>
								View general stats here
				';
				*/
				echo '
							</div>
						</div>
					</div>
				';
			}

		?>

	</div>
	<?php include "includes/footer.php"; ?>
</body>
</html>